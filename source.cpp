#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include "tree.h"

using namespace std;

bool inVector(vector<int>randomNum,int num) { //check if it have duplicate value
	for (int i = 0; i < randomNum.size(); i++) {
		if (randomNum[i] == num) {
			return true;
		}
	}
	return false;
}

int main()
{
	srand(time(0));
	Tree<int> mytree;
	vector<int>randomNum;
	int num;
	int i = 0 ;


	while (i < 10000) { 
		num = rand();
		if (!inVector(randomNum,num)) {  //insert the number which is not duplicate
			mytree.insert(num);
			randomNum.push_back(num);
			i++;
		}
	}

	mytree.inorder();
	cout << endl << endl << endl;
	cout << "Height of tree : " << mytree.depth();
	cout << endl << endl << endl;

	mytree.clear();
	mytree.balance(randomNum, 0, randomNum.size() - 1);
	cout << "After balance height of tree is : " << mytree.depth();
	cout << endl << endl << endl;
	cout << "How many insertion operations you want?";

	int insert;
	cin >> insert;
	i = 0;
	while (i < insert) {
		num = rand();
		if (!inVector(randomNum, num)) {  //insert the number which is not duplicate
			mytree.insert(num);
			randomNum.push_back(num);
			i++;
			cout << "\nInsert : " << num;
		}
	}

	int find;
	cout << endl << endl << endl;
	cout << "The number you want to find : ";
	cin >> find;
	if (mytree.Search(find)) {
		cout << "Number is found";
	}
	else {
		cout << "Number is not found";
	}

	int del;
	cout << endl << endl << endl;
	cout << "How many number you want to delete? ";
	cin >> del;
	i = 0;
	while (i < del) {
		num = rand();
		if (!inVector(randomNum, num)) {  //insert the number which is not duplicate
			mytree.findAndDelete(num);
			i++;
			cout << "\nDelete : " << num;
		}
	}

	

	
}